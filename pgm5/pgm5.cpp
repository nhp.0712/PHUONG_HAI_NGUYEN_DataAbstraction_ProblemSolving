// Program 3	(Due: Monday, Febrary 27, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Linky, the STL list mascot has been kidnapped. Rumor has it that clues 
//				to find Linky are hidden in the list classes in the C++ Standard Template.
//				The program will need to search the lists to find the combination to a safety deposit box
//				whose contents will reveal Linky's location. Include the STL list header file.
//				Name the program file pgm5.cpp.  
#include<iostream>
#include<string>
#include<list>
#include<iomanip>
#include<ctime>
using namespace std;

// Function Prototypes
char Clue1(list<string>);
int Clue2(list<char>, char);
int Clue3(list<int>, int );
unsigned long Clue4(list<unsigned long>, int); 

// Main Fucntion
int main()
{
	// create four lists
	list<string> listClue1;					// one to hold strings(e.g., list<string> listClue1;)
	list<char> listClue2;					// one to hold chars
	list<int> listClue3;					// one to hold ints
	list<unsigned long> listClue4;			// one to hold unsigned long values.
	
	//************************************* CLUE 1 **************************************************
	cout << "##-------------------------------##" << endl;
	cout << "	  Clue 1 List" << endl;
	cout << "-----------------------------------" << endl;
	char clue1 = Clue1(listClue1);					// Call the Clue1 function to display the output
	cout << "Letter of 'Linky' to look for is '" << clue1 << "'" << endl;
	cout << "##-------------------------------##" << endl << endl;
	//************************************* END CLUE 1 **************************************************



	//************************************* CLUE 2 **************************************************====
	cout << "##-------------------------------##" << endl;
	cout << "	  Clue 2 List" << endl;
	cout << "-----------------------------------" << endl;
	int clue2 = Clue2(listClue2, clue1);			// Call the Clue2 function to display the output
	if (clue2 == -1)
	{
		cout << "The letter '" << clue1 << "'" << " is not found in the list. Try again!!!" << endl;

		return 0;
	}
	cout << "Clue 2 - last letter '" << clue1 << "'" << " found in position " << clue2 << endl;
	cout << "##-------------------------------##" << endl << endl;
	//************************************* END CLUE 2 **************************************************



	//************************************* CLUE 3 **************************************************====
	cout << "##-------------------------------##" << endl;
	cout << "	  Clue 3 List" << endl;
	cout << "-----------------------------------" << endl;
	int clue3 = Clue3(listClue3, clue2);			// Call the Clue3 function to display the output
	cout << "-----------------------------------" << endl;
	cout << "Clue 3 - number found in position " << clue2 << " (from end) is " << clue3 << endl;
	cout << "##-------------------------------##" << endl << endl;
	//************************************* END CLUE 3 **************************************************



	//************************************* CLUE 4 **************************************************====
	cout << "##-------------------------------##" << endl;
	cout << "	  Clue 4 List" << endl;
	cout << "-----------------------------------" << endl;
	unsigned long clue4 = Clue4(listClue4, clue3);	// Call the Clue4 function to display the output
	cout << "-----------------------------------" << endl;
	cout << "Clue 4 - the " << clue3 << " largest number and combination" << endl;
	cout << "for the safe deposit box is " << clue4 << endl;
	cout << "##-------------------------------##" << endl << endl;
	//************************************* END CLUE 4 **************************************************

	
	return 0;
}

// Function:	Clue1
// Description: Search the first list of strings for the name "Linky". 
//				Find the remainder of dividing the position in the list where the name "Linky" 
//				is found by the length of the name (5). Use this value to determine which 
//				character in the name to search for in the second list
// Return:		char
char Clue1(list<string> listClue1)
{
	// Declare variables as needed
	srand((int)time(0));
	string Linky = "Linky";
	int position = -1;
	int count = 1;

	// Create an array of words
	string message[] = { "The", "secret", "to", "getting", "ahead", "is", "getting", "started", "Mark", "Twain" };

	// Using for loop to push randomly selected words from the message array onto the list 10 times
	for (int i = 0; i < 10; i++)
		listClue1.push_front(message[rand() % 10]);

	// Select one of the list items randomly (rand() % 10) 
	list<string>::iterator itor = listClue1.begin();
	for (int random = rand() % 10; random > 0; random--)
		itor++;

	// Put "Linky" in that location in the list in place of the word stored there.
	listClue1.insert(itor, "Linky");
	listClue1.erase(itor);

	// Using for loop to declare an interator and display all the elements in the list

	for (list<string>::iterator itor = listClue1.begin(); itor != listClue1.end(); ++itor)
	{
		if (*itor == Linky)					// Using if statement to search the first list of strings for the name "Linky".  
			position = count;
		if (count < 10)
			cout << count++ << right << setw(15) << *itor << endl;
		else
			cout << count++ << right << setw(14) << *itor << endl;
	}
	cout << "-----------------------------------" << endl;

	cout << "Clue 1 - 'Linky' found in location " << position << " in first list." << endl;

	position = position % 5;

	return Linky[position];
}

// Function:	Clue2
// Description: Search the second list of characters for the character found by the first function.
//				If there is more than one occurrence of that char, always take the last.  
//				The position of this last occurrence is where you will look for the next clue.  
//				If the letter is not found in the list, print a message telling the user to try again and exit the program.  
// Return:		int
int Clue2(list<char> listClue2, char clue1)
{
	// Declare variables as needed
	srand((int)time(0));
	int location = -1;
	int count = 1;

	// Define a char array with the letters "Linky"
	char Linky[] = { 'L', 'i', 'n', 'k', 'y' };

	// Randomly select a letter each time to push on to the list
	for (int i = 0; i < 10; i++)
		listClue2.push_front(Linky[rand() % 5]);

	// Using for loop to declare an interator and display all the elements in the list
	for (list<char>::iterator itor = listClue2.begin(); itor != listClue2.end(); ++itor)
	{
		if (*itor == clue1)					// Using if statement to search the second list of characters
			location = count;				// for the character found by your first function
		if (count < 10)
			cout << count++ << right << setw(15) << *itor << endl;
		else
			cout << count++ << right << setw(14) << *itor << endl;
	}
	cout << "-----------------------------------" << endl;

	return location;
}

// Function:	Clue3
// Description: Using the position from the previous clue, find the integer located that many positions 
//				from the end of the third list. The integer value found at that location will be used for 
//				the final search for the safety deposit combination.
// Return:		int
int Clue3(list<int> listClue3, int location)
{
	// Declare variables as needed
	srand((int)time(0));
	int number = -1;
	int count = 1;

	// Add randomly selected numbers from 0 and 24 until the list contains 10 numbers.
	for (int i = 0; i < 10; i++)
		listClue3.push_front(rand() % 25); 

	// Using for loop to declare an interator and display all the elements in the list
	for (list<int>::iterator itor = listClue3.begin(); itor != listClue3.end(); ++itor)
	{
		if (count == location)				// Using if statement find the integer located that
			number = *itor;					// many positions from the end of the third list.
		if (count < 10)
			cout << count++ << right << setw(15) << *itor << endl;
		else
			cout << count++ << right << setw(14) << *itor << endl;
	}
	
	return number;
}

// Function:	Clue4
// Description: Find the nth largest number in the fourth list where  
//				n represents the integer value found by your third function
// Return:		unsigned long
unsigned long Clue4(list<unsigned long> listClue4, int number)
{
	// Declare variables as needed
	srand((int)time(0));
	int combination = -1;
	int count = 1;

	// Add random unsigned long integers to the list until the list contains 25 numbers
	// Use this formula to generate the numbers:  (unsigned long) rand() * 123456789 + 100000000
	for (int i = 0; i < 25; i++)
		listClue4.push_front((unsigned long)rand() * 123456789 + 100000000);

	listClue4.sort();						// Sort the list
	listClue4.reverse();					// Then reverse the order of the list

	// Using for loop to declare an interator and display all the elements in the list
	for (list<unsigned long>::iterator itor = listClue4.begin(); itor != listClue4.end(); ++itor)
	{
		if (count == number)				// Using if statement to find the nth largest number in the fourth list where
			combination = *itor;			// n represents the integer value found by your third function
		if(count < 10)
			cout << count++ << right << setw(20) << *itor << endl;
		else
			cout << count++ << right << setw(19) << *itor << endl;
	}

	return combination;
}
