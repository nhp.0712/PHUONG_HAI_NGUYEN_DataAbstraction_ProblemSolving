// Program 4	(Due: Monday, Febrary 20, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Write a program to read in and evaluate a postfix expression, 
//				using an ArrayStack object of type double to hold the operands. 
//				Name the program pgm4.cpp and access the ArrayStack class by using 
//				the statement #include <lib/ArrayStack.h> in the file.
#include<iostream>
#include<string>
#include<fstream>
#include"lib/ArrayStack.h"
#include"lib/StackInterface.h"
using namespace std;

// Function Prototypes
void expression(string, ArrayStack<double> );
bool check(string);
bool IsOperator(char );
bool IsNum(char );
bool decimalNum(char );

// Main Function
int main()
{
	// Define an ArrayStack object of type double to hold the operands
	ArrayStack<double> aStack;

	string input = "";				// Declare variables as needed
	ifstream inFile;				// inFile has been declared as an ifstream object
	inFile.open("lib/pgm4.txt");		// Open the file in the class library
	
	// Using a loop so the user can enter expressions one after another until the user decides to quit('q').
	while (input != "q")
	{
		// Display output
		cout << "Enter a postfix expression and press return (enter 'q' to quit): ";
		getline(inFile, input);
		cout << input << endl;

		if (input != "q")
			expression(input, aStack);
		else
			cout << "Exit the program!" << endl;
	
	}

	return 0;
}

// Function:	expression
// Description: If the character is a number or decimal point then read in the number and push it onto the stack.
//				_If the character is one of the arithmetic operators(+, -, *, / ) :
//				_Get the top two numbers from the stack - these will be the right and left operands in theoperation.
//				_Combine these operands using the arithmetic operator : result = leftOperand  operator rightOperand;
//				_Push the result back onto the stack.
// Return:		void
void expression(string input, ArrayStack<double> aStack)
{
	// Start calculating the expression if valid
	if (check(input))
	{
		// Using for loop to go through each of character in the expression
		for (unsigned int i = 0; i < input.length(); i++)
		{
			// Using if/else if statement to ignore spaces and distinguish type of characters
			if (input[i] == ' ')
				continue;
			else if (IsOperator(input[i]))			// If the character is one of the arithmetic operators (+, -, *, /)
			{
				// Get the top two numbers from the stack - the right and left operands in the operation
				double rightOperand = aStack.peek();
				aStack.pop();
				double leftOperand = aStack.peek();
				aStack.pop();

				// Using if/else if statements to implement the expressions using the arithmetic operator: 
				// result = leftOperand  operator rightOperand
				if (input[i] == '+')
					aStack.push(leftOperand + rightOperand);
				else if (input[i] == '-')
					aStack.push(leftOperand - rightOperand);
				else if (input[i] == '*')
					aStack.push(leftOperand * rightOperand);
				else if (input[i] == '/')
					aStack.push(leftOperand / rightOperand);
			}
			else if (IsNum(input[i]))				// If the character is a number or decimal point then 
			{										// read in the number and push it onto the stack.
				double operand = 0;
				while (i < input.length() && IsNum(input[i]))
				{
					// Check if the operand is a multi-digits number or decimal number
					// then make an appropriate calculation to get the actual operand
					if (input[i] != '.')
					{
						operand = (operand * 10) + (input[i] - '0');
						if (decimalNum(input[i + 1]))
						{
							double digit = input[i + 2] - '0';
							operand = (operand * 10.00 + digit) / 10.00;
							i = i + 2;
						}
					}
					else if (input[i] == '.')
					{
						double digit = input[i + 1] - '0';
						operand = digit / 10.00;
						i = i + 1;
					}
					i++;
				}
				aStack.push(operand);				// push it onto the stack.
			}
		}
		// Display the result
		cout << "That evaluates to " << aStack.peek() << endl << endl;
	}
}

// Function:	IsNum
// Description: If the character is a number or decimal point, return true
// Return:		bool
bool IsNum(char character)
{
	if ((character >= '0' && character <= '9') || (character == '.'))
		return true;

	return false;
}

// Function:	IsOperator
// Description: If the character is one of the arithmetic operators (+, -, *, /), return true
// Return:		bool
bool IsOperator(char character)
{
	if ((character == '+') || (character == '-') || (character == '*') || (character == '/'))
		return true;

	return false;
}

// Function:	decimalNum
// Description: If the character is decimal point (in specific), return true
// Return:		bool
bool decimalNum(char character)
{
	if (character == '.')
		return true;

	return false;
}

// Function:	check
// Description: check the postfix expressions to handle invalid postfix expressions.  
//				For each invalid expression, display an error message describing the
//				type of error found in the expression, then return false if invalid
// Return:		bool
bool check(string input)
{
	// Declare variables as needed
	int totalSymbol = 0;
	int totalNum = 0;
	bool check = true;

	// Using for loop to go through each of character in the expression
	for (unsigned int i = 0; i < input.length(); i++)
	{
		// Counting the number of operands and operators.
		if (input[i] >= '0' && input[i] <= '9')
			++totalNum;
		else if ((input[i] == '+') || (input[i] == '-') || (input[i] == '*') || (input[i] == '/'))
			++totalSymbol;

		// Using if statement to check if the expression is missing operand
		if (totalNum == totalSymbol)
		{
			cout << "Invalid expression - missing operand. Try again!" << endl << endl;
			check = false;
			break;
		}

		// Using if statement to check if the expression has an invalid operation symbol
		if (!IsOperator(input[i]) && !IsNum(input[i]) && input[i] != ' ')
		{
			cout << "'" << input[i] << "' is an invalid operation symbol. Try again!" << endl << endl;
			check = false;
			++totalSymbol;
			break;
		}
	}

	// Using if statement to check if the expression is missing operator
	if ((totalNum > totalSymbol + 2) || (totalNum != 1 && totalSymbol == 0))
	{
		cout << "Invalid expression - missing operator. Try again!" << endl << endl;
		check = false;
	}

	return check;
}