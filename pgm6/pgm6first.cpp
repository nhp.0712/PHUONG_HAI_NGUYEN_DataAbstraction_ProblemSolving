// Program 6	(Due: Wednesday, March 22, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Compare two versions of the quicksort algorithm, one that uses the median of 3 strategy for choosing a pivot value 
//				to partition an array and one that simply picks the first value in the array as the pivot

#include <iostream>
#include <string>
#include <ctime>
using namespace std;

//*********************  prototypes  *********************
template<class ItemType>
void quickSort(ItemType ary[], int first, int last, int &count);

template<class ItemType>
int partition(ItemType ary[], int first, int last, int &count);

template<class ItemType>
void order(ItemType ary[], int i, int j);

template<class ItemType>
int sortFirstMiddleLast(ItemType ary[], int first, int last);

template<class ItemType>
void insertionSort(ItemType ary[], int first, int last);

template<class ItemType>
void CreateArray(ItemType ary[], int size);

template<class ItemType>
void display(ItemType ary[], int size);
// Smallest size array quicksort will sort 
static const int MIN_SIZE = 10;
static const int SIZE = 50;
static const int MAX_SIZE = 100;
//************************ main *************************
int main()
{
	srand((int)time(0));
	int ary1[MIN_SIZE];
	int ary2[SIZE];
	int ary3[MAX_SIZE];
	//**********************  First Array *************************
	int count = 0;
	CreateArray(ary1, MIN_SIZE);
	cout << "================================ First Array ==================================" << endl;
	cout << "Array (size of 10) before sorting: " << endl;
	display(ary1, MIN_SIZE);
	quickSort(ary1, 0, MIN_SIZE - 1, count);
	cout << endl << "Array (size of 10) after sorting: " << endl;
	display(ary1, MIN_SIZE);
	cout << endl << "Number of comparisons: " << count << endl << endl << endl;

	//**********************  First Array *************************


	//**********************  Second Array *************************
	count = 0;
	CreateArray(ary2, SIZE);
	cout << "================================ Second Array =================================" << endl;
	cout << "Array (size of 50) before sorting: " << endl;
	display(ary2, SIZE);
	quickSort(ary2, 0, SIZE - 1, count);
	cout << endl << "Array (size of 50) after sorting: " << endl;
	display(ary2, SIZE);
	cout << endl << "Number of comparisons: " << count << endl << endl << endl;

	//**********************  Second Array *************************


	//**********************  Third Array *************************
	count = 0;
	CreateArray(ary3, MAX_SIZE);
	cout << "================================ Third Array ==================================" << endl;
	cout << "Array (size of 100) before sorting: " << endl;
	display(ary3, MAX_SIZE);
	quickSort(ary3, 0, MAX_SIZE - 1, count);
	cout << endl << "Array (size of 100) after sorting: " << endl;
	display(ary3, MAX_SIZE);
	cout << endl << "Number of comparisons: " << count << endl << endl;

	//**********************  Third Array *************************



	system("pause");
	return 0;
}

//**********************  quickSort *************************
// Sorts an array into ascending order. Uses quick sort with
// median-of-three pivot selection for arrays of at least
// MIN_SIZE entries, uses insertion sort for other arrays.
//***********************************************************
template<class ItemType>
void quickSort(ItemType ary[], int first, int last, int &count)
{
	if (last - first + 1 < MIN_SIZE)
	{
		insertionSort(ary, first, last);
	}
	else
	{
		// Create the partition: S1 | Pivot | S2
		int pivotIndex = partition(ary, first, last, count);


		// Sort subarrays S1 and S2
		quickSort(ary, first, pivotIndex - 1, count);
		quickSort(ary, pivotIndex + 1, last, count);
	}

}


//**********************  partition *************************
// Partitions entries in an array about a pivot entry
//***********************************************************
template<class ItemType>
int partition(ItemType ary[], int first, int last, int &count)
{
	// Choose pivot using first selection
	int pivotIndex = sortFirstMiddleLast(ary, first, last);
	// Reposition pivot so it is last in the array
	swap(ary[pivotIndex], ary[last]);
	pivotIndex = last;
	ItemType pivot = ary[pivotIndex];

	// Determine the regions S1 and S2
	int indexFromLeft = first;
	int indexFromRight = last - 1;

	bool done = false;
	while (!done)
	{

		count++;							// counts the total number of comparisons 
		// Locate first entry on left that is >= pivot
		while (ary[indexFromLeft] < pivot)
		{
			indexFromLeft = indexFromLeft + 1;
			count++;						// counts the total number of comparisons 
		}

		// Locate first entry on right that is <= pivot
		while (ary[indexFromRight] > pivot)
		{
			indexFromRight = indexFromRight - 1;
			count++;						// counts the total number of comparisons 
		}
		if (indexFromLeft < indexFromRight)
		{
			std::swap(ary[indexFromLeft], ary[indexFromRight]);
			indexFromLeft = indexFromLeft + 1;
			indexFromRight = indexFromRight - 1;
		}
		else
			done = true;
	}

	// Place pivot in proper position between S1 and S2, and 
	// mark its new location
	swap(ary[pivotIndex], ary[indexFromLeft]);
	pivotIndex = indexFromLeft;

	return pivotIndex;
}

//****************  sortFirstMiddleLast ********************
// Arranges first, middle, last entry in sorted order.
//***********************************************************
template<class ItemType>
int sortFirstMiddleLast(ItemType ary[], int first, int last)
{
	int mid = first + (last - first) / 2;
	order(ary, first, mid); // Make ary[first] <= ary[mid]
	order(ary, mid, last);  // Make ary[mid] <= ary[last]
	order(ary, first, mid); // Make ary[first] <= ary[mid]

	return first;
}

//************************  order  **************************
// Arranges two specified array entries into sorted order by
// exchanging them, if necessary.
//***********************************************************
template<class ItemType>
void order(ItemType ary[], int i, int j)
{
	if (ary[i] > ary[j])
		swap(ary[i], ary[j]); // Exchange entries
}

//********************** insertionSort **********************
// Sorts the items in an array into ascending order.
// Initially, sorted region is ary[0], unsorted region is 
// ary[1..n-1]. In general, sorted region is 
// ary[0..unsorted-1], unsorted region ary[unsorted..n-1]
//***********************************************************
template<class ItemType>
void insertionSort(ItemType ary[], int first, int last)
{
	int unsorted;      // first index of the unsorted region,
	int loc;           // index of insertion in sorted region
	ItemType nextItem; // next item in the unsorted region

	for (unsorted = first + 1; unsorted <= last; unsorted++)
	{
		// ary[0..unsorted-1] sorted. Find right position
		// (loc) in ary[0..unsorted] for ary[unsorted],
		// first entry in unsorted region;
		// shift, if necessary, to make room
		nextItem = ary[unsorted];
		loc = unsorted;
		while ((loc > first) && (ary[loc - 1] > nextItem))
		{
			// Shift ary[loc - 1] to the right
			ary[loc] = ary[loc - 1];
			loc--;
		}

		// Insert nextItem in sorted region at ary[loc]
		ary[loc] = nextItem;
	}
}

//********************** CreateArray **********************
// fill an array with random integers from 1 to 9999 
// (rand() % 10000 + 1)
//***********************************************************
template<class ItemType>
void CreateArray(ItemType ary[], int size)
{
	for (int i = 0; i < size; i++)
		ary[i] = rand() % 10000 + 1;
}

//********************** display **********************
// display an array of any size
//***********************************************************
template<class ItemType>
void display(ItemType ary[], int size)
{
	for (int i = 0; i < size; i++)
		if (i < size - 1)
			cout << ary[i] << ", ";
		else
			cout << ary[i];
	cout << endl;
}