#include<iostream>
#include<string>
#include<fstream>
#include "BinaryTreeInterface.h"
#include "BinarySearchTree.cpp"
#include "BinaryNode.cpp"
#include "BinaryNodeTree.cpp"
#include "NotFoundException.cpp"
#include "PrecondViolatedExcep.cpp"

using namespace std;

int main()
{
	BinaryNodeTree<string> Node;
	BinarySearchTree<string> Search;
	ifstream infile;
	infile.open("pgm8.txt");

	BinaryNode<string> * ptr = nullptr;
	if (infile)
	{
		string data;
		while (!infile.eof())
		{
			infile >> data;
			Node.add(data);
			Search.add(data);
		}
	}
	else
		cout << "Opened file error !!" << endl;

	Node.displayVisualTree(ptr, 12);

	system("pause");
	return 0;
}