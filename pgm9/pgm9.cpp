// Program 9	(Due: Wednesday, April 26, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: This program will evaluate the data from collisions that occur as records are added 
//				to a table using different hashing algorithms, collision resolution algorithms and table sizes.

#include<iostream>
#include<string>
#include<ctime>
#include "hashTable.cpp"
#include "hashTable.h"
using namespace std;

static const int DEFAULT_CAPACITY = 107;

// Declare a structure called Inventory
struct Inventory
{
	int key;					// an int representing the inventory id
	string description;			// a string representing the description of an ID
};

int main()
{
	srand(time(NULL));
	hashTable<Inventory> HT;	// Declare a hashTable object with Inventory as the data type 
	Inventory items;			// Declare a structure object

	// Declare variables as needed
	int records = 0, value = 0, TotalColl = 0, Coll = 0, HighestColl = 0;		
	double AveColl = 0;

	// Prompt the user for the number of records to add to the hash table.
	cout << " Enter the number of records to add to the hash table: ";
	cin >> records;
	bool found = true;

	// Use this number to control a loop to add the specified number of inventory items.
	for (int i = 0; i < records; i++)
	{
		items.key = rand() % 9000 + 1000;				// generate a random number for a key (inventory ID) between 1000 and 9999
		items.description = "Inv " + to_string(i);		// Generate "descriptions" in the form Inv xx where xx is replaced by a number for each inventory item
		Coll = HT.add(items);							// Calling add function to add items and return collision number of each item
		TotalColl += Coll;								// Calculate the total of collisions
		if (HighestColl < Coll)							// Using if statement to find the highest number of collisions
			HighestColl = Coll;
	}
	AveColl = (double)TotalColl / records;				// Calculate the average collisions per item

	// Display the Hash Table as required
	cout << endl << "     Inventory Records" << endl;
	cout << "Index     Key       Description" << endl;
	HT.displayTable();
	cout << "Enter the key value to find: ";
	cin >> value;

	// Using if/else statement to check and find the value entered from user, then display an appropriate message or information of the item
	if (!HT.is_present(value))
		cout << "The key value " << value << " is not found in the record" << endl;
	else
	{
		HT.getItem(value, found, items);
		cout << "The key value " << items.key << " is found in the record with description " << items.description << endl;
	}

	// Display the results in a format as required
	cout << endl << "Knuth hashing method with double probing" << endl << endl;
	cout << "Hash table length: " << DEFAULT_CAPACITY << endl;
	cout << "Number of records added: " << records << endl;
	cout << "Most collisions for record: " << HighestColl << endl;
	cout << "Average collisions per record: " << AveColl << endl;
	cout << "Total number of collisions: " << TotalColl << endl;

 	system("pause");
	return 0;
}