// Program 2	(Due: Monday, January 30, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Create a client program named pgm2main.cpp to test the ArrayBag class 
//				and using union and intersection methods to the class ArrayBag

#include<iostream>
#include<vector>
#include"pgm2bag.h"
#include"pgm2bag.cpp"
using namespace std;

// Function Prototypes
void displayBag(ArrayBag<int>& );

int main()
{
	// Declare variables as needed
	int value = 0;
	// Define four ArrayBag objects that can hold integers
	ArrayBag<int> bag[4];

	// Using for loop to add values to two of the bags by prompting the user for input.
	for (int i = 0; i < 2; i++)
	{
		cout << "Add integer values to Bag " << i + 1 << " (type -1 in the last to end adding): ";
		while (value != -1)
		{
			cin >> value;
			if (value != -1)
				bag[i].add(value);
		}
		value = 0;
	}
	cout << endl << "==========================================================" << endl;

	// Using for loop to call displayBag function to display its current size and a list of its contents of each bag.
	for (int i = 0; i < 4; i++)
	{
		cout << "Bag " << i + 1 << " :" << endl;
		displayBag(bag[i]);
	}

	// Display the number of 8s and the number of 9s in the second bag.
	cout << "The number of 8s in the Bag 2 is: " << bag[1].getFrequencyOf(8) << endl;
	cout << "The number of 9s in the Bag 2 is: " << bag[1].getFrequencyOf(9) << endl;
	cout << "==========================================================" << endl;

	// Call Union function to create the union of the first two bags, using the third bag to hold the result
	bag[2] = bag[2].Union(bag[0], bag[1]);

	// Call the displayBag function again for each of the first three bags.
	cout << "Bag 1: " << endl;
	displayBag(bag[0]);
	cout << "Bag 2: " << endl;
	displayBag(bag[1]);
	cout  << "==> Create a union of Bag 1 and Bag 2, then store the result in Bag 3:" << endl << endl;
	displayBag(bag[2]);

	// Display the number of 8s and the number of 9s in the third bag.
	cout << "The number of 8s in the Bag 3 is: " << bag[2].getFrequencyOf(8) << endl;
	cout << "The number of 9s in the Bag 3 is: " << bag[2].getFrequencyOf(9) << endl << endl;

	// Using if/else statement to check to see if the third bag contains any 5s and display the result.
	if(bag[2].contains(5))
		cout << "The Bag 3 contains: " << bag[2].getFrequencyOf(5) << " elements of 5s"<< endl;
	else
		cout << "The Bag 3 does not contain any 5s!!" << endl;
	cout << "==========================================================" << endl;

	// Call Intersection function to create the intersection of the first two bags, using the forth bag to hold the result
	bag[3] = bag[3].Intersection(bag[0], bag[1]);

	// Call the displayBag function for the first two bags and the fourth bag.
	cout << "Bag 1: " << endl;
	displayBag(bag[0]);
	cout  << "Bag 2: " << endl;
	displayBag(bag[1]);
	cout  << "==> Create a intersection of Bag 1 and Bag 2, then store the result in Bag 4:" << endl << endl;
	displayBag(bag[3]);
	
	// Display the number of 8s and the number of 9s in the fourth bag
	cout << "The number of 8s in the Bag 4 is: " << bag[3].getFrequencyOf(8) << endl;
	cout << "The number of 9s in the Bag 4 is: " << bag[3].getFrequencyOf(9) << endl << endl;

	// Empty the fourth bag and then display its size.
	cout << "==> Empty the Bag 4." << endl << endl;

	// Call clear function to empty the forth bag.
	bag[3].clear();
	cout << "The current size of bag 4 is " << bag[3].getCurrentSize() << ". The bag is empty!!" << endl ;
	cout << "==========================================================" << endl;

	return 0;
}

// Function:	displayBag
// Description: display the current size and contents of the ArrayBag object passed to it.
// Return:		void
void displayBag(ArrayBag<int>& bag)
{
	std::vector<int> bagItems = bag.toVector();
	if ((int)bagItems.size() > 0)
	{
		cout << "The current size of this Bag is " << bag.getCurrentSize() << " and its contents: ";
		for (int i = 0; i < (int)bagItems.size(); i++)
			cout << bagItems[i] << " ";
		cout << endl << endl;
	}
	else
		cout << "The current size of bag is " << bag.getCurrentSize() << ". The bag is empty!!" << endl << endl;
} 