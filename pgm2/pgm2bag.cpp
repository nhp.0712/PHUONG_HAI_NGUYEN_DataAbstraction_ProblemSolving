// Program 2	(Due: Monday, January 30, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description:	Implementation file for the class ArrayBag. @file ArrayBag.cpp 

// Created by Frank M. Carrano and Timothy M. Henry.
// Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.



#include "pgm2bag.h"
#include <cstddef>
#include <vector>

template<class ItemType>
ArrayBag<ItemType>::ArrayBag(): itemCount(0), maxItems(DEFAULT_CAPACITY)
{
}  // end default constructor

template<class ItemType>
int ArrayBag<ItemType>::getCurrentSize() const
{
	return itemCount;
}  // end getCurrentSize

template<class ItemType>
bool ArrayBag<ItemType>::isEmpty() const
{
	return itemCount == 0;
}  // end isEmpty

template<class ItemType>
bool ArrayBag<ItemType>::add(const ItemType& newEntry)
{
	bool hasRoomToAdd = (itemCount < maxItems);
	if (hasRoomToAdd)
	{
		items[itemCount] = newEntry;
		itemCount++;
	}  // end if
    
	return hasRoomToAdd;
}  // end add

template<class ItemType>
bool ArrayBag<ItemType>::remove(const ItemType& anEntry)
{
   int locatedIndex = getIndexOf(anEntry);
	bool canRemoveItem = !isEmpty() && (locatedIndex > -1);
	if (canRemoveItem)
	{
		itemCount--;
		items[locatedIndex] = items[itemCount];
	}  // end if
    
	return canRemoveItem;
}  // end remove

template<class ItemType>
void ArrayBag<ItemType>::clear()
{
	itemCount = 0;
}  // end clear

template<class ItemType>
int ArrayBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const
{
   int frequency = 0;
   int curIndex = 0;       // Current array index
   while (curIndex < itemCount)
   {
      if (items[curIndex] == anEntry)
      {
         frequency++;
      }  // end if
      
      curIndex++;          // Increment to next entry
   }  // end while
   
   return frequency;
}  // end getFrequencyOf

template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const
{
	return getIndexOf(anEntry) > -1;
}  // end contains


template<class ItemType>
std::vector<ItemType> ArrayBag<ItemType>::toVector() const
{
   std::vector<ItemType> bagContents;
	for (int i = 0; i < itemCount; i++)
		bagContents.push_back(items[i]);
      
   return bagContents;
}  // end toVector

// private
template<class ItemType>
int ArrayBag<ItemType>::getIndexOf(const ItemType& target) const
{
	bool found = false;
   int result = -1;
   int searchIndex = 0;
   
   // If the bag is empty, itemCount is zero, so loop is skipped
   while (!found && (searchIndex < itemCount))
   {
      if (items[searchIndex] == target)
      {
         found = true;
         result = searchIndex;
      } 
      else
      {
         searchIndex++;
      }  // end if
   }  // end while
   
   return result;
}  // end getIndexOf

template<class ItemType>
 // Function:		Union
 // Description:	Create the union of two bags and return an ArrayBag object that holds the result
 // Return:			ArrayBag object
ArrayBag<ItemType> ArrayBag<ItemType>::Union(ArrayBag<ItemType> bag1, ArrayBag<ItemType> bag2) const
{
	ArrayBag<ItemType> unionBag;
	
	std::vector<int> bag1Items = bag1.toVector();
	std::vector<int> bag2Items = bag2.toVector();
	for (int i = 0; i < (int)bag1Items.size(); i++)		// Using for loop to add all the contents of 2 bags into unionBag
		unionBag.add(bag1Items[i]);
	for (int i = 0; i < (int)bag2Items.size(); i++)
		unionBag.add(bag2Items[i]);
	return unionBag;									// Return unionBag
} 

template<class ItemType>
// Function:	Intersection
// Description: Create the intersection of two bags and return an ArrayBag object that holds the result
// Return:		ArrayBag object
ArrayBag<ItemType> ArrayBag<ItemType>::Intersection(ArrayBag<ItemType> bag1, ArrayBag<ItemType> bag2) const
{
	ArrayBag<ItemType> unionBag;
	std::vector<int> bag1Items = bag1.toVector();
	for (int i = 0; i < (int)bag1Items.size(); i++)		// Using for loop to add all the same contents of 2 bags into unionBag
	{
		if (bag2.contains(bag1Items[i]))				// Using if statement to check if the contents of 2 bags are the same, if yes, add into unionBag
			unionBag.add(bag1Items[i]);
	}
	return unionBag;									// Return unionBag
}

/* ALTERNATE 1: First version
template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& target) const 
{
   return getFrequencyOf(target) > 0;
}  // end contains

// ALTERNATE 2: Second version 
template<class ItemType>
bool ArrayBag<ItemType>::contains(const ItemType& anEntry) const
{
   bool found = false;
   int curIndex = 0;        // Current array index
   while (!found && (curIndex < itemCount))
   {
      if (anEntry == items[curIndex])
      {
         found = true;
      } // end if
      
      curIndex++;           // Increment to next entry
   }  // end while   
   
   return found;
}  // end contains
*/

