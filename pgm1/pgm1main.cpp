// Program 1	(Due: Wednesday, January 18, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description:	use C++ inheritance to create a base class and two derived classes and working with templates.  

#include<iostream>
#include<iomanip>
#include"pgm1BasicShape.h"
#include"pgm1Circle.h"
#include"pgm1Rectangle.h"
using namespace std;

// Main function
int main()
{
	// Declare variables as needed
	double width = 0, length = 0, centerX = 0, centerY = 0, radius = 0, area = 0;
	
	// Asking user to enter (integer) values for the object's data for Circle
	cout << "Please enter the x coordinate of the circle's center (int): ";
	cin >> centerX;
	cout << "Please enter the y coordinate of the circle's center (int): ";
	cin >> centerY;
	cout << "Please enter the radius of the circle: ";
	cin >> radius;

	// Create an Circle object initialized with these (integer) values.  
	Circle <int> iCir(area, centerX, centerY, radius);

	// Display the values (using the class methods) for the object.
	cout << "The x and y coordinates of the circle (int) are " << iCir.getCenterX() << " and " << iCir.getCenterY() << endl;
	cout << "The area of the circle is " << iCir.getArea() << endl << endl;



	// Asking user to enter (double) values for the object's data for Cirlce
	cout << "Please enter the x coordinate of the circle's center (double): ";
	cin >> centerX;
	cout << "Please enter the y coordinate of the circle's center (double): ";
	cin >> centerY;
	cout << "Please enter the radius of the circle: ";
	cin >> radius;

	// Create an Circle object initialized with these (double) values. 
	Circle <double> dCir(area, centerX, centerY, radius);

	// Display the values (using the class methods) for the object.
	cout << "The x and y coordinates of the circle (double) are " << dCir.getCenterX() << " and " << dCir.getCenterY() << endl;
	cout << "The area of the circle is " << dCir.getArea() << endl << endl;



	// Asking user to enter (integer) values for the object's data for Rectangle
	cout << "Please enter the width of the rectangle (int): ";
	cin >> width;
	cout << "Please enter the length of the rectangle (int): ";
	cin >> length;

	// Create an Rectangle object initialized with these (integer) values. 
	Rectangel <int> iRec(area, width, length);

	// Display the values (using the class methods) for the object.
	cout << "The width and length of the rectangle (int) are " << iRec.getWidth() << " and " << iRec.getLength() << endl;
	cout << "The area of the rectangle is " << iRec.getArea() << endl << endl;



	// Asking user to enter (double) values for the object's data for Rectangle
	cout << "Please enter the width of the rectangle (double): ";
	cin >> width;
	cout << "Please enter the length of the rectangle (double): ";
	cin >> length;

	// Create an Rectangle object initialized with these (double) values. 
	Rectangel <double> dRec(area, width, length);

	// Display the values (using the class methods) for the object.
	cout << "The width and length of the rectangle (double) are " << dRec.getWidth() << " and " << dRec.getLength() << endl;
	cout << "The area of the rectangle is " << dRec.getArea() << endl << endl;

	
	return 0;
}