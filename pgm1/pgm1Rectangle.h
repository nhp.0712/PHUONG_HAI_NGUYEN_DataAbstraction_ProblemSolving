// Program 1	(Due: Wednesday, January 18, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description:	use C++ inheritance to create a base class and two derived classes and working with templates.  

// Guard preprocessor statements
#ifndef PGM1_RECTANGLE
#define PGM1_RECTANGLE
#include<iostream>
#include"pgm1BasicShape.h"
using namespace std;

// Template prefix 
template<class ItemType>

// Declare another class named Rectangle that is also derived from the BasicShape class 
class Rectangel : public BasicShape<ItemType>
{
	// Private data members
private:
	ItemType width;				// an integer used to hold the width of the rectangle
	ItemType length;			// an integer used to hold the length of the rectangle

	// Public member functions:
public:
	// A parameterized constructor that accepts values for width and length
	Rectangel(double area, const ItemType& width, const ItemType& length)
	{
		this->width = width;
		this->length = length;
		// Call the orverriden calcArea function and stores the result in the inherited data member area.
		area = calcArea();
		this->setArea(area);
	};

	ItemType getWidth() const { return width; };		// returns the value in width
	ItemType getLength() const { return length; };		// returns the value in length

	// Overriden function calculates the area of the rectangle.
	double calcArea() const { return (length * width); };
};
#endif
