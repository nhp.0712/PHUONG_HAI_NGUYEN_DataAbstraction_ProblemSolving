// Program 1	(Due: Wednesday, January 18, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description:	use C++ inheritance to create a base class and two derived classes and working with templates.  

// Guard preprocessor statements
#ifndef PGM1_CIRCLE
#define PGM1_CIRCLE
#include<iostream>
#include"pgm1BasicShape.h"
using namespace std;

// Template prefix 
template<class ItemType>

// Declare a class named Circle that is derived from the BasicShape class
class Circle : public BasicShape<ItemType>
{
	// Private data members
private:
	ItemType centerX;				// an integer used to hold the x coordinate of the circle's center.
	ItemType centerY;				// an integer used to hold the y coordinate of the circle's center.
	double radius;					// a double used to hold the circle's radius.

	// Public member functions:
public:

	// A parameterized constructor that accepts values for centerX, centerY and radius
	Circle(double area, const ItemType& centerX, const ItemType& centerY, double radius)
	{
		this->centerX = centerX;
		this->centerY = centerY;
		this->radius = radius;
		// Call the orverriden calcArea function and stores the result in the inherited data member area.
		area = calcArea();
		this->setArea(area);
	};

	ItemType getCenterX() const { return centerX; };	// returns the value in centerX
	ItemType getCenterY() const { return centerY; };	// returns the value in centerY

	// Overriden function calculates the area of the circle.
	double calcArea() const { return (3.14159 * radius * radius); };
};
#endif
