// Program 1	(Due: Wednesday, January 18, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description:	use C++ inheritance to create a base class and two derived classes and working with templates.  

// Guard preprocessor statements
#ifndef PGM1_BASICSHAPE
#define PGM1_BASICSHAPE
#include<iostream>
using namespace std;

// Template prefix 
template<class ItemType>

// Declare a pure abstract base class called BasicShape with a private data member area, a double used to hold the shape's area.  The class has two public member functions:
class BasicShape
{
	// Private data member
private:
	double area;

	// Public member functions:
public:
	// Accessor functions
	double getArea() const { return area; };			// Returns the value in the member variable area.
	void setArea(double area) { this->area = area; };	// Accepts a value and stores it in the private data member area. 
	virtual double calcArea() const = 0;				// A pure virtual function.
};
#endif