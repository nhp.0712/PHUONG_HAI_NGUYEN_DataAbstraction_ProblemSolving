// Program 7	(Due: Monday, April 3, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Using a queue to simulate the flow of customers through a checkout line at the local grocery store and report the simulation results
//				For the simulation you must model both the passage of time and the flow of customers through the line.  
//				You can model time using a loop in which each repetition of the loop corresponds to a set time interval.  
//				Assume that each pass through the loop represents 1 minute and the simulation should be run for some number of minutes 
//				(set the length of the simulation to 30 minutes).To simulate the flow of customers through the line you need a data structure to hold information about each customer.
//				Since a queue is a FIFO(first - in, first - out) data structure it will work well for modeling the customers waiting in the line.Customers are added(enqueued) 
//				at the end of the line and served or waited on from the front of the line(dequeued).
#include<iostream>
#include<ctime>
#include<cstdlib>
#include "ArrayQueue.h"
using namespace std;

// Structure customer
//
struct customer
{
	int ID = 0;				// customer ID
	int timeline = 0;		// time the customer joined the line
	int timetrans = 0;		// transaction time 
};

// Constant integer
const int simulation = 30;

// ** Main function **
int main()
{
	// Declare and initialize variables as needed
	srand((int)time(0));
	customer Customer;						// declare structure object
	ArrayQueue<customer> cstQueue;			// declare queue object 
	int randNum = rand() % 3 + 1;			// generate numbers in the range 1 to 3 use rand()
	int trans[simulation];
	int move = 0, LongestWait = 0, count = 0;
	double AveWait = 0, totalWait = 0, wait = 0;
	for (int i = 0; i < simulation; i++)
		trans[i] = 0;

	// Using a for loop in which each repetition of the loop corresponds to a set time interval
	for (int i = 0; i < simulation; i++)				// each pass through the loop represents 1 minute and the simulation 
	{													// should be run for some number of minutes (set the length of the simulation to 30 minutes).
		randNum = rand() % 3 + 1;
		cout << "-------------------------------------------------------------" << endl;
		if (randNum == 1)								// If the random number is 1, add a customer to the line.  
		{
			Customer.ID++;
			Customer.timeline = i + 1;
			Customer.timetrans = rand() % 5 + 1;
			cstQueue.enqueue(Customer);
			trans[Customer.ID - 1] = Customer.timetrans;	// Using an array to store transaction time of new customer while waiting

			// Display the minute value and information about customers added
			cout << "Minute " << i + 1 << ": Added customer " << Customer.ID << " at minute " << i + 1 << ", transaction time " << Customer.timetrans << endl;
		}
		else											// If the random number is 2 or 3, no customer is added to the line.  
			cout << "Minute " << i + 1 << ": No customers added to queue at time " << i + 1 << endl;


		if (!cstQueue.isEmpty())					
		{	// Using if statement to check if this is the first serving at the first transaction time to display exactly wait time and added time
			if (trans[move] == cstQueue.peekFront().timetrans) 
			{
				if (Customer.ID == 1)					// Check if it is the first customer
				{
					cout << "Serving customer " << cstQueue.peekFront().ID << " added at time " << cstQueue.peekFront().timeline << " wait time was 0" << endl;
					trans[move]--;										// Decreasing transaction time each time served
				}
				else
				{
					wait = i + 1 - cstQueue.peekFront().timeline;		// Calculate the waiting time
					if (wait > LongestWait)
						LongestWait = wait;								// Store the longest waiting time
					totalWait += wait;									// Store the total of waiting time to find average afterward
					cout << "Serving customer " << cstQueue.peekFront().ID << " added at time " << cstQueue.peekFront().timeline << " wait time was " << wait << endl;
					trans[move]--;										// Decreasing transaction time each time served
				}
			}

			else if (trans[move] != 0)	// Display customer being served during transaction
			{
				trans[move]--;												// Decreasing transaction time each time served
				cout << "Serving customer " << cstQueue.peekFront().ID << endl;
			}


			if (trans[move] == 0)		// Customer leaves after transaction is done
			{
				move++;
				cstQueue.dequeue();
			}
		}
	}

	// ** Display the summary statistics about the simulation **
	cout << "-------------------------------------------------------------" << endl;
	cout << "Simulation Time - " << simulation << " Minutes" << endl;
	cout << "Total customers served  ";

	if (!cstQueue.isEmpty())
	{
		cout << cstQueue.peekFront().ID - 1 << endl;
		AveWait = totalWait / (double)cstQueue.peekFront().ID;		// Calculate average waiting time just in 30 mins (if there customers still waiting)
	}
	else
	{
		cout << Customer.ID << endl;
		AveWait = totalWait / (double)Customer.ID;					// Calculate average waiting time just in 30 mins (if all of customers are served)
	}
	int CustomerLeft = 0;
	
	// Using while loop to count the number of customer left (if not empty)
	while (!cstQueue.isEmpty())
	{
		cstQueue.dequeue();
		CustomerLeft++;
	}
	cout << "Customers still waiting " << CustomerLeft << endl;
	cout << "Average wait time       " << AveWait << endl;
	cout << "Longest wait time       " << LongestWait << endl;
	cout << "----------------------------------------------------------" << endl;
	system("pause");
	return 0;
}

 