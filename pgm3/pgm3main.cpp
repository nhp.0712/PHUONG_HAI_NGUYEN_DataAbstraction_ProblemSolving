// Program 3	(Due: Monday, Febrary 13, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Using the LinkedBag class as defined in Chapter 4 of the Carrano & Henry textbook, create a spell checker.  
//				Create a "dictionary" by reading correctly spelled words from a file and storing them in a LinkedBag object. 
//				Read words from a second file of words to be checked and put them in another LinkedBag.  
//				Then add a new method to the LinkedBag class that creates a bag containing the incorrectly spelled words. 
//				This is the difference of the two bags, the (misspelled) words left after removing those (correctly spelled) that were found in the dictionary. 
//				The misspelled words should be added to the bag in order by their length with the shorter misspelled words at the beginning of the list.
#include<iostream>
#include<string>
#include<vector>
#include<fstream>
#include<iomanip>
#include "lib/BagInterface.h"
#include "lib/Node.h"
#include "pgm3bag.h"
#include "pgm3bag.cpp"

using namespace std;

// Function Prototypes
void displayBag(LinkedBag<string>&);

// Main Function
int main()
{
	// Define three LinkedBag objects that can hold strings.
	LinkedBag<string> bagDict, bagCheck, bagMisspelled;

	ifstream inFile;					//Assuming inFile has been declared as an ifstream object
	inFile.open("lib/dictionary.txt");		// Open the file in the class library
	string word = "";					// Declare variables as needed

	// Using while loop to read in words from the files and add to bags.
	inFile >> word;
	while (!inFile.eof())
	{
		bagDict.add(word);
		inFile >> word;
	}
	inFile.close();						// Close the file

	inFile.open("lib/checkWords.txt");		// Open the file in the class library

	// Using while loop to read in words from the files and add to bags.
	inFile >> word;
	while (!inFile.eof())
	{
		bagCheck.add(word);
		inFile >> word;
	}
	inFile.close();						// Close the file

	// Display output
	cout << right << setw(35) << "Dictionary Bag " << endl;
	displayBag(bagDict);				// Call the display function for Dictionary Bag 
	cout << right << setw(35) << "CheckWords Bag " << endl;
	displayBag(bagCheck);				// Call the display function for CheckWords Bag
	cout << right << setw(43) << "Incorrectly spelled words Bag " << endl;
	cout << right << setw(40) << "(in order by its length) " << endl;
	bagDict.spellcheck(bagCheck, bagMisspelled);
	displayBag(bagMisspelled);			// Call the display function for Incorrectly spelled words Bag

	return 0;
}

// Function:	displayBag
// Description: display its current size and a list of its contents
// Return:		void
void displayBag(LinkedBag<string> &bag)
{
	std::vector<string> bagItems = bag.toVector();
	if ((int)bagItems.size() > 0)
	{
		cout << "=======================================================" << endl;
		cout << "The number of words in the list is " << bag.getCurrentSize() << " and its contents: " << endl;
		for (int i = 0; i < (int)bagItems.size(); i++)
			cout << bagItems[i] << endl;
		cout << "=======================================================" << endl;
		cout << endl;
	}
	else
		cout << "The number of words in the list is " << bag.getCurrentSize() << ". The list is empty!!" << endl << endl;
}

