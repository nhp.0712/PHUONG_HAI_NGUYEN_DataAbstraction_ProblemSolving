// Program 3	(Due: Monday, Febrary 13, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Using the LinkedBag class as defined in Chapter 4 of the Carrano & Henry textbook, create a spell checker.  
//				Create a "dictionary" by reading correctly spelled words from a file and storing them in a LinkedBag object. 
//				Read words from a second file of words to be checked and put them in another LinkedBag.  
//				Then add a new method to the LinkedBag class that creates a bag containing the incorrectly spelled words. 
//				This is the difference of the two bags, the (misspelled) words left after removing those (correctly spelled) that were found in the dictionary. 
//				The misspelled words should be added to the bag in order by their length with the shorter misspelled words at the beginning of the list.

// Created by Frank M. Carrano and Timothy M. Henry.
// Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

/** ADT bag: Link-based implementation.
    @file LinkedBag.h 
    Listing 4-3 */
#ifndef LINKED_BAG_
#define LINKED_BAG_

#include "lib/BagInterface.h"
#include "lib/Node.h"
#include <vector>
template<class ItemType>
class LinkedBag : public BagInterface<ItemType>
{
  private:
     Node<ItemType>* headPtr; // Pointer to first node
     int itemCount;           // Current count of bag items
   
     // Returns either a pointer to the node containing a given entry
     // or the null pointer if the entry is not in the bag.
     Node<ItemType>* getPointerTo(const ItemType& target) const;
   
  public:
     LinkedBag();
     LinkedBag(const LinkedBag<ItemType>& aBag); // Copy constructor
     virtual ~LinkedBag();                       // Virtual destructor
     int getCurrentSize() const;
     bool isEmpty() const;
     bool add(const ItemType& newEntry);
     bool remove(const ItemType& anEntry);
     void clear();
     bool contains(const ItemType& anEntry) const;
     int getFrequencyOf(const ItemType& anEntry) const;
     std::vector<ItemType> toVector() const;
	 void spellcheck(LinkedBag<ItemType> &, LinkedBag<ItemType>&);
};


#endif
