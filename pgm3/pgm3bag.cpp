// Program 3	(Due: Monday, Febrary 13, 2017)
// Author:		Phuong Hai Nguyen
// Class:		CS3350 (Class time: 6:00 PM)
// Description: Using the LinkedBag class as defined in Chapter 4 of the Carrano & Henry textbook, create a spell checker.  
//				Create a "dictionary" by reading correctly spelled words from a file and storing them in a LinkedBag object. 
//				Read words from a second file of words to be checked and put them in another LinkedBag.  
//				Then add a new method to the LinkedBag class that creates a bag containing the incorrectly spelled words. 
//				This is the difference of the two bags, the (misspelled) words left after removing those (correctly spelled) that were found in the dictionary. 
//				The misspelled words should be added to the bag in order by their length with the shorter misspelled words at the beginning of the list.

// Created by Frank M. Carrano and Timothy M. Henry.
// Copyright (c) 2017 Pearson Education, Hoboken, New Jersey.

/** ADT bag: Link-based implementation.
    @file LinkedBag.cpp */

#include "pgm3bag.h"
#include<iostream>
#include <vector>
#include <string>
#include <cstddef>

// --------------------------------------------------
//                Default Constructor
// --------------------------------------------------
template<class ItemType>
LinkedBag<ItemType>::LinkedBag() : headPtr(nullptr), itemCount(0)
{
}

// --------------------------------------------------
//                   Copy  Constructor
// --------------------------------------------------
template<class ItemType>
LinkedBag<ItemType>::LinkedBag(const LinkedBag<ItemType>& aBag)
{
   itemCount = aBag.itemCount;
   Node<ItemType>* origChainPtr = aBag.headPtr;  // Points to nodes in original chain
   
   if (origChainPtr == NULL)
      headPtr = NULL;  // Original bag is empty
   else
   {
      // Copy first node
      headPtr = new Node<ItemType>();
      headPtr->setItem(origChainPtr->getItem());
      
      // Copy remaining nodes
      Node<ItemType>* newChainPtr = headPtr;      // Points to last node in new chain
      origChainPtr = origChainPtr->getNext();     // Advance original-chain pointer
      
      while (origChainPtr != NULL)
      {
         // Get next item from original chain
         ItemType nextItem = origChainPtr->getItem();
              
         // Create a new node containing the next item
         Node<ItemType>* newNodePtr = new Node<ItemType>(nextItem);
         
         // Link new node to end of new chain
         newChainPtr->setNext(newNodePtr);
         
         // Advance pointer to new last node
         newChainPtr = newChainPtr->getNext();

         // Advance original-chain pointer
         origChainPtr = origChainPtr->getNext();
      }  // end while
      
      newChainPtr->setNext(NULL);              // Flag end of chain
   }  // end if
}

// --------------------------------------------------
//                 Destructor
// --------------------------------------------------
template<class ItemType>
LinkedBag<ItemType>::~LinkedBag()
{
   clear();
}

// --------------------------------------------------
//                    isEmpty
// --------------------------------------------------
template<class ItemType>
bool LinkedBag<ItemType>::isEmpty() const
{
   return itemCount == 0;
}  

// --------------------------------------------------
//                 getCurrentSize
// --------------------------------------------------
template<class ItemType>
int LinkedBag<ItemType>::getCurrentSize() const
{
   return itemCount;
}  

// --------------------------------------------------
//                      add
// --------------------------------------------------
template<class ItemType>
bool LinkedBag<ItemType>::add(const ItemType& newEntry)
{
   // Add to beginning of chain: new node references rest of chain;
   // (headPtr is null if chain is empty)        
   Node<ItemType>* nextNodePtr = new Node<ItemType>();
   nextNodePtr->setItem(newEntry);
   nextNodePtr->setNext(headPtr);  // New node points to chain

   headPtr = nextNodePtr;          // New node is now first node
   itemCount++;
   
   return true;
}

// --------------------------------------------------
//                    toVector
// --------------------------------------------------
template<class ItemType>
std::vector<ItemType> LinkedBag<ItemType>::toVector() const
{
   std::vector<ItemType> bagContents;
   Node<ItemType>* curPtr = headPtr;
   int counter = 0;
   while ((curPtr != nullptr) && (counter < itemCount))
   {
      bagContents.push_back(curPtr->getItem());
      curPtr = curPtr->getNext();
      counter++;
   }  
   
   return bagContents;
}

// --------------------------------------------------
//                    remove
// --------------------------------------------------
template<class ItemType>
bool LinkedBag<ItemType>::remove(const ItemType& anEntry)
{
   Node<ItemType>* entryNodePtr = getPointerTo(anEntry);
   bool canRemoveItem = !isEmpty() && (entryNodePtr != nullptr);
   if (canRemoveItem)
   {
      // Copy data from first node to located node
      entryNodePtr->setItem(headPtr->getItem());
      
      // Delete first node
      Node<ItemType>* nodeToDeletePtr = headPtr;
      headPtr = headPtr->getNext();
      
      // Return node to the system
      nodeToDeletePtr->setNext(NULL);
      delete nodeToDeletePtr;
      nodeToDeletePtr = NULL;
      
      itemCount--;
   }
   
	return canRemoveItem;
}  

// --------------------------------------------------
//                    clear
// --------------------------------------------------
template<class ItemType>
void LinkedBag<ItemType>::clear()
{
   Node<ItemType>* nodeToDeletePtr = headPtr;
   while (headPtr != nullptr)
   {
      headPtr = headPtr->getNext();

      // Return node to the system
      nodeToDeletePtr->setNext(nullptr);
      delete nodeToDeletePtr;
      
      nodeToDeletePtr = headPtr;
   }                       // headPtr, nodeToDeletePtr now nullptr
   
   itemCount = 0;
}  

// --------------------------------------------------
//                  getFrequencyOf
// --------------------------------------------------
template<class ItemType>
int LinkedBag<ItemType>::getFrequencyOf(const ItemType& anEntry) const
{
	int frequency = 0;
   int counter = 0;
   Node<ItemType>* curPtr = headPtr;
   while ((curPtr != nullptr) && (counter < itemCount))
   {
      if (anEntry == curPtr->getItem())
      {
         frequency++;
      } // end if
      
      counter++;
      curPtr = curPtr->getNext();
   } // end while
   
   return frequency;
}  

// --------------------------------------------------
//                    contains
// --------------------------------------------------
template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
   return (getPointerTo(anEntry) != nullptr);
}  


// --------------------------------------------------
//                    getPointerTo
// --------------------------------------------------
// private
// Returns either a pointer to the node containing a given entry 
// or the null pointer if the entry is not in the bag.
template<class ItemType>
Node<ItemType>* LinkedBag<ItemType>::getPointerTo(const ItemType& anEntry) 
const
{
   bool found = false;
   Node<ItemType>* curPtr = headPtr;
   
   while (!found && (curPtr != nullptr))
   {
      if (anEntry == curPtr->getItem())
         found = true;
      else
         curPtr = curPtr->getNext();
   }
   
   return curPtr;
} 

// Function:	spellcheck
// Description: The function should check each word in the bag of words to be checked to see if it is spelled correctly 
//				(found in the dictionary bag). If a word is spelled incorrectly, add it to the bag of incorrectly spelled words
//				in order by its length. Shorter misspelled words should appear earlier in the list and longer misspelled words 
//				towards the end of the list.
// Return:		void

template<class ItemType>
void LinkedBag<ItemType>::spellcheck(LinkedBag<ItemType>& bagCheck, LinkedBag<ItemType>& bagMisspelled)
{
	std::vector<ItemType> bagItems = bagCheck.toVector();

	std::string Temp = "";
	bool swap = true;

	// Using do/while loop to sort the list of words by length.
	do
	{
		swap = false;
		for (int i = 0; i < (int)bagItems.size() - 1; i++)
		{
			if (bagItems[i].length() < bagItems[i + 1].length())
			{
				Temp = bagItems[i];
				bagItems[i] = bagItems[i + 1];
				bagItems[i + 1] = Temp;

				swap = true;
			}
		}
	} while (swap);

	// Using for loop to check if a word is spelled incorrectly, add it to the bag of incorrectly spelled words 
	for (int i = 0; i < (int)bagItems.size(); i++)		
	{
		if (!LinkedBag<ItemType>::contains(bagItems[i]))
			bagMisspelled.add(bagItems[i]);
	}
}

// --------------------------------------------------
//                      add
//            ALTERNATE for 1st 3 lines
// --------------------------------------------------
//   Node<ItemType>* nextNodePtr = new Node<ItemType>(newEntry, headPtr); 

// --------------------------------------------------
//                    contains
//            ALTERNATE 1 implementation
// --------------------------------------------------
/*
template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
   return getFrequencyOf(anEntry) > 0;
} 
*/

// --------------------------------------------------
//                    contains
//            ALTERNATE 2 implementation
// --------------------------------------------------
// ALTERNATE 2 
/*
template<class ItemType>
bool LinkedBag<ItemType>::contains(const ItemType& anEntry) const
{
   bool found = false;
   Node<ItemType>* curPtr = headPtr;
   int i = 0;
   while (!found && (curPtr != nullptr) && (i < itemCount))
   {
      if (anEntry == curPtr-<getItem())
      {
         found = true;
      }
      else
      {
         i++;
         curPtr = curPtr->getNext();
      }  // end if
   }  // end while

   return found;
}  // end contains
*/


