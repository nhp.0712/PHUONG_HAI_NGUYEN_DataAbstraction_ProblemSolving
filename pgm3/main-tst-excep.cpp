// ------------------------------------------------
// Program to test array-based stack implementation
// Compile with exception class file 
//   g++ main-tst-excep.cpp PrecondViolatedExcep.cpp
// ------------------------------------------------
#include <iostream>
#include <string>
#include "ArrayStackExcep.h"

// -------------------------------------
//                main
// -------------------------------------
int main()
{
   ArrayStack<std::string> stk;

   if (stk.isEmpty())
      std::cout << "\nStack is empty\n\n";
   else
      std::cout << "\nStack has entries\n\n";

   try
   {
      std::string item = stk.peek();
      std::cout << "Item on stack = " << item << "\n";
   }
   catch (PrecondViolatedExcep e)
   {
      std::cout << e.what();
   }

   std::cout << "\n";
   return 0;
}

